#include <Arduino.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include <NeoPixelBus.h>

#define NUM_LEDS 12
#define BUFFER_LEN 1024
#define PRINT_FPS 1

const uint8_t PixelPin = 13;

const char* ssid     = "Playmedia";
const char* password = "12345678";
unsigned int localPort = 7777;
char packetBuffer[BUFFER_LEN];

uint8_t NUM_PIX = 0;

WiFiUDP port;
IPAddress ip(192, 168, 1, 200);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

NeoPixelBus<NeoGrbFeature, Neo800KbpsMethod> ledstrip(NUM_LEDS, PixelPin);

void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.config(ip, gateway, subnet);
  WiFi.begin(ssid, password);
  Serial.println("");

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  port.begin(localPort);

  ledstrip.Begin();
  ledstrip.Show();
}

#if PRINT_FPS
uint16_t fpsCounter = 0;
uint32_t secondTimer = 0;
#endif

void loop() {

  int packetSize = port.parsePacket();
  if (packetSize) {
    int len = port.read(packetBuffer, BUFFER_LEN);
    for (int i = 0; i < len; i += 4) {
      packetBuffer[len] = 0;
      NUM_PIX = packetBuffer[i];
      RgbColor pixel((uint8_t)packetBuffer[i + 1], (uint8_t)packetBuffer[i + 2], (uint8_t)packetBuffer[i + 3]); //color
      ledstrip.SetPixelColor(NUM_PIX, pixel);
    }
    ledstrip.Show();
#if PRINT_FPS
    fpsCounter++;
    Serial.print("/");
#endif
  }
#if PRINT_FPS
  if (millis() - secondTimer >= 1000U) {
    secondTimer = millis();
    Serial.printf("FPS: %d\n", fpsCounter);
    fpsCounter = 0;
  }
#endif
}
