# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(['config.py','dsp.py','microphone.py','visualization.py','melbank.py','gui.py'],
             pathex=['C:\\Users\\Eunbiline_98\\Documents\\project LKI\\dynalite-project\\Research RGB Led Strip Arduino\\Program\\audio reactive led strip\\python'],
             binaries=[],
             datas=[],
             hiddenimports=['C:\\Users\\Eunbiline_98\\Documents\\project LKI\\dynalite-project\\Research RGB Led Strip Arduino\\Program\\audio reactive led strip\\python','pkg_resources.py2_warn'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='main',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='main')
